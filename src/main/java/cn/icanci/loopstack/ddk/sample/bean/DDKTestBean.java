package cn.icanci.loopstack.ddk.sample.bean;

import cn.icanci.loopstack.ddk.common.anno.DdkValue;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * {String.class, Integer.class, int.class, Long.class, long.class, Double.class,
 *         double.class, Float.class, float.class, Boolean.class, boolean.class, Character.class, 
 *         char.class, Short.class, short.class, Byte.class, byte.class, BigDecimal.class}
 *         
 * @author icanci
 * @since 1.0 Created in 2023/01/14 15:30
 */
@SuppressWarnings("all")
@Service
public class DDKTestBean implements InitializingBean {
    @DdkValue("#test.string")
    private String     testString;
    @DdkValue("#test.long")
    private Long       testLong;
    @DdkValue("#test.int")
    private Integer    testInteger;
    @DdkValue("#test.double")
    private Double     testDouble;
    @DdkValue("#test.float")
    private Float      testFloat;
    @DdkValue("#test.boolean")
    private Boolean    testBoolean;
    @DdkValue("#test.char")
    private Character  testCharacter;
    @DdkValue("#test.short")
    private Short      testShort;
    @DdkValue("#test.byte")
    private Byte       testByte;
    @DdkValue("#test.long")
    private long       testLongUnbox;
    @DdkValue("#test.int")
    private int        testIntegerUnbox;
    @DdkValue("#test.double")
    private double     testDoubleUnbox;
    @DdkValue("#test.float")
    private float      testFloatUnbox;
    @DdkValue("#test.boolean")
    private boolean    testBooleanUnbox;
    @DdkValue("#test.char")
    private char       testCharacterUnbox;
    @DdkValue("#test.short")
    private short      testShortUnbox;
    @DdkValue("#test.byte")
    private byte       testByteUnbox;
    @DdkValue("#test.bigDecimal")
    private BigDecimal testBigDecimal;

    @Override
    public void afterPropertiesSet() throws Exception {
        Thread thread = new Thread(() -> {
            while (true) {
                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(10));
                System.out.println("DDKTestBean#afterPropertiesSet");
                System.out.println("testString：" + testString);

                System.out.println("testLong：" + testLong);
                System.out.println("testInteger：" + testInteger);
                System.out.println("testDouble：" + testDouble);
                System.out.println("testFloat：" + testFloat);
                System.out.println("testBoolean：" + testBoolean);
                System.out.println("testShort：" + testShort);
                System.out.println("testByte：" + testByte);

                System.out.println("testLongUnbox：" + testLongUnbox);
                System.out.println("testIntegerUnbox：" + testIntegerUnbox);
                System.out.println("testDoubleUnbox：" + testDoubleUnbox);
                System.out.println("testFloatUnbox：" + testFloatUnbox);
                System.out.println("testBooleanUnbox：" + testBooleanUnbox);
                System.out.println("testShortUnbox：" + testShortUnbox);
                System.out.println("testByteUnbox：" + testByteUnbox);

                System.out.println("testBigDecimal：" + testBigDecimal);
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
}

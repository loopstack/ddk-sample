package cn.icanci.loopstack.ddk.sample.bean;

import cn.icanci.loopstack.ddk.common.anno.DdkAfterCall;
import cn.icanci.loopstack.ddk.common.anno.DdkBeforeCall;
import cn.icanci.loopstack.ddk.common.anno.DdkCall;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/14 15:43
 */
@Service
//@DdkResource("#or.trade.core.timeout#limit")
public class DDKTestResource2 {
    @DdkBeforeCall
    private void ddkBeforeCall(String one) {
        System.out.println("DDKTestResource#ddkBeforeCall");
        System.out.println(one);
    }

    @DdkCall
    private void ddkCall(String one) {
        System.out.println("DDKTestResource#ddkCall");
        System.out.println(one);
    }

    @DdkAfterCall
    private void ddkAfterCall(String one) {
        System.out.println("DDKTestResource#ddkAfterCall");
        System.out.println(one);
    }
}

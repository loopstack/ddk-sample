package cn.icanci.loopstack.ddk.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 18:22
 */
@SpringBootApplication
public class DDKSampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(DDKSampleApplication.class, args);
    }
}
